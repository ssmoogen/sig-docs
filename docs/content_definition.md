# Content definition

The primary deliverable of the Automotive SIG is AutoSD, an RPM repository
that you can use to build Automotive images. AutoSD is, essentially,
CentOS Stream, with some divergences that represent unique Automotive
SIG content.

For example, divergences might include packages not present in
CentOS Stream or packages that override CentOS Stream with differently built
or configured packages for situations that require deviations.

The Automotive SIG relies on
[Content Resolver](https://tiny.distro.builders/)
to define and select content, using CentOS Stream and CBS repositories
as its dependency-resolution backends.

## Definition layout

Content Resolver operates with four distinct input-definition types:

* Repositories
* Environments
* Workloads
* Views

**Repositories** group one or more package repositories and prioritize
them for dependency resolution. The SIG uses CentOS Stream
repositories and CBS and COPR as fallbacks. With this mechanism, we can
include packages that are not currently part of CentOS Stream.

**Environments** represent the minimum recommended installation
footprint. They list top-level, standard packages expected in
every in-vehicle installation. The Automotive environment includes a
set of packages required to boot the system on the target hardware
platform, as well as additional components identified during functional
safety and security requirements reviews.

**Workloads** layer additional packages on top of the
essential in-vehicle environment or extend the Automotive repository
with new content potentially useful for development and testing. The
two standard, defined workloads are the in-vehicle and the off-vehicle
sets. The in-vehicle workload is empty and only inherits the environment
component list. The off-vehicle workload extends the environment
component list with various development tools. Additional experimental workloads
can be added to visualize and analyze the impact of including new packages.

**Views** unify the resolved workloads into a single, flat
list of packages. The view effectively represents the Automotive
package repository and can be used to build them in practice.

For more information about these input types, see the
[Content Resolver README](https://github.com/minimization/content-resolver#content-resolver) on GitHub.

## Adding your own content

Adding new content to the deliverable is easy. The Auto SIG recommends
defining a new workload to extend the default environment. Definitions are managed on
[GitHub](https://github.com/minimization/content-resolver-input).

Use the `automotive` label to inherit the base in-vehicle environment
automatically and include the newly added package components into the
unified view. Only top-level packages must be listed, because the service
automatically resolves dependencies. Dependencies can be
listed explicitly as well, if your application depends on them.

If the workload requires content not present in CentOS Stream or CBS, add
additional repositories to the Automotive repository definition. Use a
lower priority for the custom repositories to avoid masking content
that comes from CentOS Stream or CBS.
