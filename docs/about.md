# The Automotive SIG

## 1. SIG Status

**Accepted**

## 2. Charter

The purpose of the Automotive SIG is two-fold.
First, it is meant to be a neutral public space for collaboration between third parties interested in open development
of software targeted at in-vehicle automotive use cases.
Second, it is meant to provide such projects with build and test infrastructure. (This second purpose is, as we understand it, common to many SIGs)

The goal of the SIG is to provide an open-source home for RHEL-oriented automotive work, and to attract and encourage open development of automotive
software between commercial and non-commercial partners. To that end, this SIG will have three primary functions:

* Create open source software related to automotive
* Incorporate upstream projects related to automotive
* Build and curate a CentOS variant for Automotive based on CentOS Stream on a regular release schedule

## 3. Why This SIG is Important

CentOS as a community project provides a wealth of enterprise-level assets and processes that fit well with a vision of the future automotive industry,
as envisioned by Red Hat and many other organizations.
Since the 1980s, automotive in-car computing systems have been constructed as discrete embedded systems known as Electronic Control Units (ECUs),
isolated by hardware and connected through a simple bus (CAN bus).
In 2011, manufacturers and suppliers helped drive the development of ISO 26262, an accepted automotive safety standard that was designed
around this design paradigm.

The world has changed since 2011, with massive increases in computing power at low power and cost footprints, strong advances in Linux,
and the introduction of edge computing and cloud-based services that previously were only available within a given computing unit.
Automotive computing is on the threshold of an evolutionary leap toward the vehicle as an edge device rather than an embedded system,
as can be seen by the introduction of many products from tier-1 providers in the industry (and from OS providers,
such as Red Hat, who previously did not participate in the embedded marketplace) and from strong interest by auto manufacturers
and the general public.

This paradigm shift creates an opportunity for Linux-based operating systems such as CentOS to lead the transition from the car as
an isolated set of embedded systems into a cohesive, intelligent edge device.
This SIG is intended to be a collaboration point for automotive Linux within this new paradigm.

## 4. Upstreams & Contributions

### 4.1. Fedora IoT Embedded Working Group

This SIG will work in close cooperation with a proposed working group under Fedora IoT and will incorporate some subset of the work done in Fedora.
It will also remain open to the incorporation of other automotive-oriented upstream projects as long as they fit with the overall charter of this
group in terms of content, community, and open-source license.

### 4.2. Other Upstreams & Contributions

We anticipate that many other upstreams will be added to the requirements for this SIG and its resulting CentOS variant over time.
In addition, we anticipate that software will be developed within this SIG.

A **Contributor Guide** is forthcoming, to outline the contribution process and acceptable licenses.

### 4.3. Community and Governance

SIG members will collaborate over mailing lists, IRC, and in regular meetings through a Technical Steering Committee (TSC) and various
individual project mailing lists as required. Meetings and assets will be open to the public.

Because this SIG is based in CentOS, a community-driven project, there are no restrictions or costs to membership in the SIG,
and leadership is recognized by participation. In general, we have conceived of the SIG as containing multiple upstream projects
bound by a common interest in automotive use cases, contributed and developed by a set of members who are able to collaborate in this context.
The standards for leadership, shared agenda, and ongoing discussions that would be driven by a steering committee are TBD.

The primary benefit of SIG membership, as currently contemplated, is access to shared resources for build and testing.
The committee, presumably, would establish some standards for what types of projects would be accepted, what reasonable
load they may place on the shared build and test resources, and how relevance and activity of projects should be tracked ongoing.

## 5. Leadership

* Technical Lead: Pierre-Yves Chibon
* Administrative Lead: Jefro Osier Mixon

## 6. Code and Documentation

* [GitLab code repositories](https://gitlab.com/CentOS/automotive)
* [Documentation](index.md)

## 7. Communications

* [CentOS Automotive mailing list](https://lists.centos.org/hyperkitty/list/automotive-sig@lists.centos.org/)
* [CentOS Development mailing list](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/)
* [#centos-automotive-sig](https://app.element.io/#/room/#centos-automotive-sig:fedoraproject.org) on Matrix

Please see the [meetings page](community/index.md#past-meetings) for the meeting schedule, dialup info, and links to recordings of past meetings.
