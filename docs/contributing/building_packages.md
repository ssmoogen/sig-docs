# Building packages for Automotive

There are three ways to contribute automotive-related packages to
AutoSD or Red Hat In-Vehicle OS:

* Package the application in Fedora and bring it to EPEL
* Package the application in the Automotive SIG RPM repository
* Contact your partner manager to discuss incorporating the application into Red Hat In-Vehicle OS

Understanding how code flows into RHEL and Red Hat In-Vehicle OS will help
clarify which contribution method is right for you:

![Code Flow into AutoSD, CentOS Stream, RHEL and the Automotive product](../img/CodeFlow.jpg)

## Building packages for Fedora and EPEL

The [Fedora project](https://docs.fedoraproject.org/en-US/project/) is an open source community
that works together on multiple projects, the most well known of which is the Fedora Linux operating system.

Red Hat Enterprise Linux (RHEL) is based on the work done in the Fedora Linux
distribution. RHEL is based on Fedora and Red Hat In-Vehicle OS is based on RHEL.

In addition to Fedora Linux, the Fedora community works on a project called
[Extra Packages for Enterprise Linux (EPEL)](https://docs.fedoraproject.org/en-US/epel/).
EPEL allows Fedora packagers to build and maintain packages built against RHEL.
Therefore, packages that are part of EPEL are necessarily compatible with RHEL, CentOS Stream, and
Red Hat In-Vehicle OS.

!!! note
    EPEL packages cannot override RHEL packages. If you want a package that already exists in RHEL
    but with a different configuration or version, you must build your package so that it
    does not conflict with RHEL packages.

For more information about how to become a Fedora packager, see
[Joining the Package Maintainers](https://docs.fedoraproject.org/en-US/package-maintainers/Joining_the_Package_Maintainers/)
on the [Fedora Project user documentation site](https://docs.fedoraproject.org/en-US/docs/).

## Building packages for the Automotive SIG RPM repository

The most important contribution criteria for the Automotive SIG repo is that the
code must be under an [approved free and open source license](https://docs.fedoraproject.org/en-US/legal/allowed-licenses/).

Similar to the Fedora Linux-RHEL relationship, when you build in the Automotive
SIG RPM repository, you're building against AutoSD, which is the future of Red Hat In-Vehicle OS.

The contribution process for the Automotive SIG RPM repository is somewhat simpler than the EPEL
process because the Automotive SIG controls the process.

Unlike EPEL, the Automotive SIG repository can override packages that are in
RHEL or AutoSD. This allows SIG members to experiment with new versions or
different configurations than those in Red Hat products.

### Requesting a package

Send an email to the [CentOS Automotive SIG](https://lists.centos.org/hyperkitty/list/automotive-sig@lists.centos.org/)
mailing list to present the project you want to package in the Automotive SIG RPM repository.

### Packaging code

After they receive your email, the Automotive SIG will create a subgroup or project for you
in the [CentOS Automotive RPMs repo](https://gitlab.com/CentOS/automotive/rpms/).

#### Prerequisites

* `lookaside_upload_sig` script from [centos-git-common](https://git.centos.org/centos-git-common/blob/master/f/lookaside_upload_sig)

* `centos-packager` to build packages using CentOS Build System [(CBS)](https://cbs.centos.org/)

#### Procedure

1. Run `git add` and `git commit` to add the spec file and patches to your repo.

2. Upload the tarball of the project sources to the lookaside cache:

   ```console
   lookaside_upload_sig -f <tarball> -n <pkg_name>
   ```

!!! note

    The lookaside cache stores a copy of all upstream archives used when building RPMs.
    This allows the Automotive SIG to reproduce a build, even if the upstream
    project's website disappears. After your sources are uploaded to the lookaside cache,
    create a `sources` file in your git repository to enable the build system to identify
    the correct archive to retrieve from the lookaside cache.

3. Create a `sources` file in your repo in the same directory as the spec file:

```console
sha512sum --tag <tarball> > sources
```

4. Push your spec file, `sources` file, and any patches to GitLab.

5. The first time you package your code, add your package to the tags you want to build
against:

```console
cbs add-pkg --owner=<username> <tag> <pkg_name>
```

!!! note

    You must do this for the `-candidate`, `-testing` and `-release` tags.
    For more information, see [Automotive SIG CBS tags](#automotive-sig-cbs-tags).

6. Build the package using CBS:

```console
cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#<git_hash>
```

Or to build the latest commit of the branch you're in:

```console
cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#`git log -1 --format=format:"%H"`
```

7. After the build succeeds, push it to the testing repository:

```console
cbs tag <testing_tag> <pkg_name-version-release>
```

Using the Automotive SIG testing repository for CentOS Stream 9 as an example:

```console
cbs tag automotive9s-packages-main-testing <pkg_name-version-release>
```

!!! note
    This makes the package show up in the [CentOS Buildlogs mirror](https://buildlogs.centos.org/9-stream/automotive/).

8. Push the package to the stable repository in the CentOS mirror network:

```console
cbs tag <release_tag> <pkg_name-version-release>
```

Using the Automotive SIG release repository for CentOS Stream 9 as an example:

```console
cbs tag automotive9s-packages-main-release <pkg_name-version-release>
```

!!! note

    This makes the package show up in the
    [CentOS Stream mirror](http://mirror.stream.centos.org/SIGs/9-stream/automotive/).

### Automotive SIG CBS tags

CBS tags classify packages in the build system based on their
destination.

The Automotive SIG RPM repository uses the following tags for CentOS Stream 9:

* **automotive9s-packages-main-el9s**: This is the tag you will specify when using the `cbs build` command.
  It corresponds to the main Automotive SIG RPM repository. It is currently our only repository.
* **automotive9s-packages-main-el9s-build**: This tag specifies where your packages are built.
* **automotive9s-packages-main-candidate**: This tag specifies where your build lands after it's built.
* **automotive9s-packages-main-testing**: This tag specifies packages destined for the testing repository.
* **automotive9s-packages-main-release**: This tag specifies packages destined for the stable repository.

And the following tags for CentOS Stream 10:

* **automotive10s-packages-main-el10s**: This is the tag you will specify when using the `cbs build` command.
  It corresponds to the main Automotive SIG RPM repository. It is currently our only repository.
* **automotive10s-packages-main-el10s-build**: This tag specifies where your packages are built.
* **automotive10s-packages-main-candidate**: This tag specifies where your build lands after it's built.
* **automotive10s-packages-main-testing**: This tag specifies packages destined for the testing repository.
* **automotive10s-packages-main-release**: This tag specifies packages destined for the stable repository.

## Building packages for AutoSD

AutoSD is the public, in-development version of the Red Hat Automotive product.
Changes that land in AutoSD are meant to land in the product in
the future. Rollbacks can happen, but they are expected to be exceptional.

Because AutoSD is the public, in-development version of the Red Hat Automotive product,
direct access is restricted to Red Hat employees. However, anyone in the community
can propose changes to AutoSD.

Change proposals can take many forms, from a
pull or merge request opened against an AutoSD package, to a post on the
[centos-automotive-sig](https://lists.centos.org/mailman/listinfo/centos-automotive-sig)
mailing list, to contacting your Red Hat partner manager.

!!! note

    The Automotive SIG recommends contacting your Red Hat
    partner manager when you open a pull or merge request.

### Finding AutoSD sources

Most AutoSD sources are located in
the [CentOS Stream repo](https://gitlab.com/redhat/centos-stream/).

* The [`rpms`](https://gitlab.com/redhat/centos-stream/rpms) namespace contains
the git repositories that constitute `dist-git`, which are the git repositories that contain the
spec files and patches used to build CentOS Stream RPMs.

* The [`src`](https://gitlab.com/redhat/centos-stream/src) namespace contains the
[Source-git](https://docs.centos.org/en-US/stream-contrib/contributing/source-git/), which is the
repository for CentOS Stream 9 and 10.

* The [`automotive`](https://gitlab.com/CentOS/automotive) namespace contains packages that diverge from CentOS Stream for AutoSD.
  The `automotive` namespace uses the same structure as the `rpms` and `src` namespaces.

!!! note

    The sources for the Automotive SIG packages may also be kept in
    [`automotive`](https://gitlab.com/CentOS/automotive). Therefore, the Automotive SIG recommends
    using different branches with clear naming for AutoSD and the Automotive SIG repos.

### Building for AutoSD

AutoSD packages are built on the CentOS Build System ([CBS](https://cbs.centos.org/)).
The procedure to build those packages is therefore similar to the procedure to build
RPMs for the Automotive SIG RPM repository. The only difference is in the CBS tags used,
which are described in [AutoSD CBS tags](#autosd-cbs-tags).

### AutoSD CBS tags

CBS tags classify packages in the build system based on their destination.

The AutoSD repository uses the following tags for CentOS Stream 9:

* **autosd9s-packages-main-el9s**: This is the tag you will specify when using the `cbs build` command.
  It corresponds to the main Automotive SIG RPM repository. It is currently our only AutoSD repository.
* **autosd9s-packages-main-el9s-build**: This tag specifies where your packages are built.
* **autosd9s-packages-main-candidate**: This tag specifies where your build lands after it's built.
* **autosd9s-packages-main-testing**: This tag specifies packages destined for the testing repository.
* **autosd9s-packages-main-release**: This tag specifies packages destined for the stable repository.

And the following tags for CentOS Stream 10:

* **autosd10s-packages-main-el10s**: This is the tag you will specify when using the `cbs build` command.
  It corresponds to the main Automotive SIG RPM repository. It is currently our only AutoSD repository.
* **autosd10s-packages-main-el10s-build**: This tag specifies where your packages are built.
* **autosd10s-packages-main-candidate**: This tag specifies where your build lands after it's built.
* **autosd10s-packages-main-testing**: This tag specifies packages destined for the testing repository.
* **autosd10s-packages-main-release**: This tag specifies packages destined for the stable repository.
