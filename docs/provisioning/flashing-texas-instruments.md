# Flashing a prebuilt AutoSD image on Texas Instruments (TI) SK-AM69 Jacinto

Follow this procedure to flash the prebuilt AutoSD image onto the SK-AM69 Jacinto board.

**Prerequisites**

* An AArch64 system with internet access
* An SD card
* A TI SK-AM69 board

**Procedure**

1. Identify the name of the latest nightly image and store the value in a variable called `AUTOSD_IMAGE_NAME`.

    ```console
    export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/TI/ | grep -oE 'auto-osbuild-am69sk-autosd9-qa-regular-aarch64-[0-9]+\.[A-Za-z0-9]+\.raw\.xz' | head -n 1)"
    ```

1. Download the image.

    ```console
    wget -O auto-osbuild-am69sk-autosd9-qa-regular.raw.xz http://autosd.sig.centos.org/AutoSD-9/nightly/TI/$AUTOSD_IMAGE_NAME
    ```

1. Uncompress the compressed `.xz` image file:

    ```console
    xz -d auto-osbuild-am69sk-autosd9-qa-regular.raw.xz
    ```

1. To flash the uncompressed image onto an SD card, replace *`</dev/sdX>`* with your block device.

    ```console
    dd if=auto-osbuild-am69sk-autosd9-qa-regular.raw of=</dev/sdX> status=progress bs=4M
    ```

1. Set the onboard DIP switches to SD boot mode `SW11[1-8] = 1000 0010 SW7[1-8] = 0000 0000`.

1. Plug in the SD card, and then start the system.

**Additional Resources**

* [TI J784S4 EVM Hardware Setup Guide](https://software-dl.ti.com/jacinto7/esd/processor-sdk-linux-j784s4/09_01_00_06/exports/docs/linux/How_to_Guides/Hardware_Setup_with_CCS/J784S4_EVM_Hardware_Setup.html)
