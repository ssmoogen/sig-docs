# Building and flashing images on NXP S32G

The process to build an AutoSD image and flash it onto a S32G board has several steps:

1. Update the board firmware.
    1. Patch the `u-boot-nodtb.bin` binary.
    2. Patch the secondary boot loader (SBL) binary.
2. Build the AutoSD image.
3. Build the NXP BSP.
    1. Optional: Install the NXP BSP kernel into the AutoSD image.
    2. Install the boot loader into the AutoSD image.
4. Flash the image onto an SD card and boot the system.

## Updating your S32G firmware

Before flashing an AutoSD image onto the S32G board, you must update the NXP firmware by installing some patches to the `u-boot-nodtb.bin` and
secondary boot loader (SBL) binaries.

### Patching the U-Boot binary

Update the `u-boot-nodtb.bin` binary and enable the default distro settings.

**Prerequisites**

* An AArch64 system with internet access
* The `make` tool
* The `patch` tool

**Procedure**

1. Clone the `u-boot` vendor repository:

    ```console
    git clone https://github.com/nxp-auto-linux/u-boot
    ```

2. Change to the `u-boot` directory:

    ```console
    cd u-boot
    ```

3. Create a patch file called `config.diff` that contains the following content:

    ```console
    cat <<EOF > config.diff
    diff --git a/configs/s32g399ardb3_defconfig b/configs/s32g399ardb3_defconfig

    index 38399e2a6e..dda5fc35e9 100644
    --- a/configs/s32g399ardb3_defconfig
    +++ b/configs/s32g399ardb3_defconfig
    @@ -1,3 +1,4 @@
    CONFIG_ARM=y
    CONFIG_ARCH_S32G3=y
    CONFIG_SD_BOOT=y
    +CONFIG_DISTRO_DEFAULTS=y
    EOF
    ```

4. Apply the patch:

    ```console
    patch -p1 < config.diff
    ```

    !!! note

        If the patch fails due to merge conflicts, then make the changes manually and build the file. Lines marked with `+` are additions; lines marked with `-` are deletions.

5. Build the `u-boot-nodtb.bin` binary:

    ```console
    make s32g399ardb3_defconfig
    make -j$(nproc)
    ```

**Next steps**
Now that the `u-boot-nodtb.bin` binary is updated, you can patch the SBL binary.

### Patching the secondary boot loader binary

After you patch the `u-boot-nodtb.bin` binary, you must also patch the secondary boot loader (SBL) binary `fip.s32`. This patch updates the values of
the `TF_LDFLAGS` and `FIP_ALIGN` parameters. The updated `fip.s32` binary is used during the
[Installing the NXP BSP into the AutoSD image](#installing-the-nxp-bsp-into-the-autosd-image) procedure.

**Prerequisites**

* An AArch64 system with internet access
* The `make` tool
* The `patch` tool
* An updated `u-boot-nodtb.bin` file

      For more information, see [Patching the u-boot-nodtb.bin binary](#patching-the-u-boot-binary)

**Procedure**

1. Clone the `arm-trusted-firmware` vendor repository:

    ```console
    git clone https://github.com/nxp-auto-linux/arm-trusted-firmware.git
    ```

2. Change to the `arm-trusted-firmware` directory:

    ```console
    cd arm-trusted-firmware
    ```

3. Create a patch file called `atf.diff` that contains the following content:

    ```console
    cat << EOF > atf.diff
    diff --git a/Makefile b/Makefile

    index e7194a73f..2c9aa6f68 100644
    --- a/Makefile
    +++ b/Makefile
    @@ -408,7 +408,7 @@ TF_LDFLAGS += $(TF_LDFLAGS_$(ARCH))
    # LD = gcc (used when GCC LTO is enabled)
    else ifneq ($(findstring gcc,$(notdir $(LD))),)
    # Pass ld options with Wl or Xlinker switches
    -TF_LDFLAGS += -Wl,--fatal-warnings -O1
    +TF_LDFLAGS += -O1
    TF_LDFLAGS += -Wl,--gc-sections
    ifeq ($(ENABLE_LTO),1)
    ifeq (${ARCH},aarch64)
    @@ -425,7 +425,7 @@ TF_LDFLAGS += $(subst --,-Xlinker --,$(TF_LDFLAGS_$(ARCH)))

    # LD = gcc-ld (ld) or llvm-ld (ld.lld) or other
    else
    -TF_LDFLAGS += --fatal-warnings -O1
    +TF_LDFLAGS += -O1
    TF_LDFLAGS += --gc-sections
    # ld.lld doesn't recognize the errata flags,
    # therefore don't add those in that case
    diff --git a/plat/nxp/s32/s32_common.mk b/plat/nxp/s32/s32_common.mk
    index 155670bdb..f862ed95a 100644
    --- a/plat/nxp/s32/s32_common.mk
    +++ b/plat/nxp/s32/s32_common.mk
    @@ -462,7 +462,7 @@ ${MKIMAGE_FIP_CONF_FILE}: ${BL2_W_DTB_SIZE_FILE} ${FIP_HDR_SIZE_FILE} add_to_fip
    T_SIZE=0x$$($(call hexbc, $${BL2_W_DTB_SIZE}, +, $${HDR_SIZE})); \
    echo "DATA_FILE SIZE $$T_SIZE" >> $@

    -FIP_ALIGN := 16
    +FIP_ALIGN := 64
    all: add_to_fip
    add_to_fip: fip ${BL2_W_DTB}
    $(eval FIP_MAXIMUM_SIZE_10 = $(shell printf "%d\n" ${FIP_MAXIMUM_SIZE}))
    EOF
    ```

4. Apply the patch:

    ```console
    patch -p1 < atf.diff
    ```

    !!! note

        If the patch fails due to merge conflicts, make the changes manually and then build the file. Lines marked with `+` are additions;
        lines marked with `-` are deletions.

5. Build the SBL binary:

    ```console
    make ARCH=aarch64 PLAT=s32g399ardb3 \
         BL33=<path-to-uboot>/u-boot-nodtb.bin
    ```

**Next steps**
Now that the `fip.s32` binary is updated, you can build the AutoSD image.

## Building and flashing an AutoSD image on S32G

To make an image to flash onto the S32G you must complete the following steps:

1. Build the AutoSD image
2. Install the secondary boot loader and optionally the NXP BSP kernel into the image

You can then flash this image onto an SD card, insert the SD card into the S32G, and boot the system.

### Building the AutoSD image

Follow these instructions to build the AutoSD image for your S32G board.

**Prerequisites**

* An AArch64 system with internet access

**Procedure**

1. Clone the `sample-images` repository:

    ```console
    git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
    ```

    !!! note

        Ensure that you include the `--recursive` option so that the clone operation includes the submodules within the repository.

2. Change to the `automotive-image-builder` directory:

    ```console
    cd automotive-image-builder
    ```

3. Build the image using your image name as the target:

    ```console
    automotive-image-builder --container build --distro autosd --mode package --target s32g_vnp_rdb3 --export image ../images/developer.mpp.yml
    autosd-s32g_vnp_rdb3-developer-regular.img
    ```

**Next steps**
Now that you have built the `autosd-s32g_vnp_rdb3-developer-regular.img` image, install the NXP kernel into it.

### Installing the NXP BSP into the AutoSD image

At the time of publication, the AutoSD image is not fully supported on the S32G board without modifications.
To create a bootable AutoSD image for the S32G board, you must manually install the secondary boot loader (SBL) binary into the AutoSD image.
In addition, the included AutoSD kernel based on CentOS Stream 9 has only partial support and not all devices and features work.
You can optionally replace it with the NXP BSP kernel.

**Prerequisites**

* An `autosd-s32g_vnp_rdb3-developer-regular.img` image; for more information, see [Building the AutoSD image](#building-the-autosd-image)
* An updated `u-boot-nodtb.bin` file; for more information, see [Patching the u-boot-nodtb.bin binary](#patching-the-u-boot-binary)
* An updated `fip.s32` file; for more information, see [Patching the secondary boot loader binary](#patching-the-secondary-boot-loader-binary)

**Procedure**

1. Optional: Build the NXP vendor kernel:

    ```console
    git clone https://github.com/nxp-auto-linux/linux.git
    cd linux
    make s32cc_defconfig
    # This command is required to boot from AutoSD default boot manager

    scripts/config -e CONFIG_EFI
    make -j$(nproc)
    ```

2. Set the helper environment variables:

    ```console
    IMAGE=../autosd-s32g_vnp_rdb3-developer-regular.img
    image=$(basename $IMAGE)
    loopdev="$(losetup --show -fP $IMAGE)"
    kernelversion="$(cat include/config/kernel.release)"
    ```

3. Create a temporary directory and mount the third and fourth partitions of the AutoSD image as block devices:

    ```console
    mkdir tmpdir
    sudo mount ${loopdev}p4 ./tmpdir/
    sudo mount ${loopdev}p3 ./tmpdir/boot/
    ```

4. Optional: Build the NXP kernel:

    ```console
    sudo make INSTALL_MOD_PATH=./tmpdir modules_install
    sudo make INSTALL_DTBS_PATH=./tmpdir/boot/dtb-${kernelversion} dtbs_install
    ```

5. Optional: Copy the NXP kernel to the AutoSD image that you mounted in `./tmpdir`:

    ```console
    sudo cp arch/arm64/boot/Image ./tmpdir/boot/
    sudo cp System.map ./tmpdir/boot/
    ```

6. Modify the boot loader:

    ```console
    rm ./tmpdir/boot/loader/*
    cat << EOF | sudo tee -a ./tmpdir/boot/loader/ffffffffffffffffffffffffffffffff-${kernelversion}.aarch64.conf
    title Automotive Stream Distribution (${kernelversion}.aarch64) 9
    version ${kernelversion}.aarch64
    linux /Image-${kernelversion}
    devicetree /dtb-${kernelversion}/freescale/s32g399a-rdb3.dtb
    options root=/dev/mmcblk0p4 rw loglevel=4 efi=noruntime acpi=off console=ttyLF0,115200 module.sig_enforce=1 systemd.show_status=auto
    libahci.ignore_sss=1 slub_debug=FPZ fsck.mode=skip rcupdate.rcu_normal_after_boot=0 rcupdate.rcu_expedited=1
    grub_users $grub_users
    grub_arg --unrestricted
    grub_class autosd
    EOF
    ```

7. Unmount the filesystem and detach the loop device:

    ```console
    sudo umount ./tmpdir/boot
    sudo umount ./tmpdir
    losetup -D ${loopdev}
    ```

8. Install the SBL binary, `fip.s32`, into the AutoSD image:

    ```console
    sudo dd if=fip.s32 of=autosd-s32g_vnp_rdb3-developer-regular.img \
        skip=512 seek=512 iflag=skip_bytes oflag=seek_bytes \
        conv=fsync,notrunc
    ```

**Next steps**
Now that you have installed the SBL binary and optionally the NXP BSP kernel into the AutoSD image, you can flash the image onto the S32G board.

### Flashing the AutoSD image onto the S32G board

After you customize the AutoSD image and install the NXP kernel and boot loader into the image, you can flash the image onto the S32G board.

**Prerequisites**

* An SD card
* An S32G board
* An `autosd-s32g_vnp_rdb3-developer-regular.img` image

**Procedure**

1. Flash the image onto an SD card, replacing `_</dev/sdX>_` with your block device:

    ```console
    sudo dd if=autosd-s32g_vnp_rdb3-developer-regular.img of={_</dev/sdX>_} \
            status=progress conv=fsync
    ```

2. Plug in the SD card and start the system.

    !!! note

        Flashed u-boot supports EFI boot, so the system should boot without any user interaction.

3. To see the output, log in to the serial port using default credentials:

    ```console
    Automotive Stream Distribution 9
    Kernel 5.15.129-rt67 on an aarch64

    localhost login: root
    Password: password
    [root@localhost ~]#
    ```
