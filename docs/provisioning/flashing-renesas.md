# Building and flashing images on Renesas R-Car S4

To flash AutoSD onto your R-Car S4 board, you must update the R-Car S4 boot firmware, host the {productshortname} image on a TFTP server on your
 workstation, and configure your R-Car S4 to communicate with the TFTP server.

**Prerequisites**

* Access to the Renesas [R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway)
* A workstation PC configured with a TFTP server
* A network connection between the PC and the R-Car S4 board

## Installing the Boot Firmware

To install the boot firmware for the R-Car S4, download the startup guide, Tera Term macros, and boot firmware files from the
 [R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway).
 These files are in the Software Downloads section and require login credentials from Renesas.

* Download the Startup Guide zip file, `R-Car_S4_SDK_start_up_guide_content_v3.16.2.zip`.
  * Extract the `R-CarS4_SDK_StartupGuide_3.16.2.zip` file from the archive.
  * This archive contains the `R-CarS4_SDK_StartupGuide.pdf` guide and the `teratermmacro` folder.
* Download the firmware files from `R-Car S4 SDK Linux PKG with GCC compiler v3.16.2`.
  * Download `xOS3_SDK1_Linux_ReferenceSDK1_v3.16.2_release.zip`
  * Unpack the archive, then unpack the included `rcar-xos_sdk1-gateway_v3.16.2_release.tar.gz` file
  * The boot firmware is in `rcar-xos/v3.16.2/os/bootloader/s4`

To install the firmware, follow the instructions in section 18 of the `R-CarS4_SDK_StartupGuide.pdf`, "How to flash images."

## Installing the AutoSD Image

To flash AutoSD onto your R-Car S4 board, you must host the AutoSD image on a TFTP server on your workstation machine and configure your R-Car S4 to
communicate with the TFTP server.

**Prerequisites**

* Boot firmware is installed and working
* A workstation PC configured with a TFTP server
* A network connection between the PC and the R-Car S4 board

### Configuring Networking in U-Boot

Configure networking on your R-Car S4 so that the board can communicate with the TFTP server that hosts your AutoSD image.

**Procedure**

1. Connect port 1 of the 3 ports on the Ethernet sub-board.

!!! note
        Identify port 1 from the PCB label (silkscreen) next to the connector. The 3 connectors are labeled CN101, CN201, CN301. Port 1 is CN101.
        You can also observe the physical layout of the board. Port 1 is farther apart from the other 2 ports, and to the right when you face the
        RJ45 connector side of the board.

2. Set the necessary networking environment variables:

    ```console
    setenv ethaddr __<xx:xx:xx:xx:xx:xx>__
    setenv ipaddr 192.168.0.20
    setenv netmask 255.255.255.0
    setenv serverip 192.168.0.1
    saveenv
    ```

    Where the values match the values for your network setup:

    * `ethaddr` specifies the MAC address from the sticker attached to the Ethernet connector
    * `ipaddr` specifies the IP address of the S4 board
    * `netmask` specifies the standard netmask
    * `serverip` specifies the IP address of the PC hosting the TFTP server

### Building the Image

The process to build AutoSD images involves the `automotive-image-builder` tool, which is available in the
[automotive-image-builder repository](https://gitlab.com/CentOS/automotive/src/automotive-image-builder). This repository contains a configuration
file for the Renesas R-Car S4 `automotive-image-builder/targets/rcar_s4.ipp.yml` that you can use to build images for your R-Car S4 board.

**Prerequisites**

* A Fedora, CentOS, or RHEL system running on AArch64 architecture
* `git`
* `gzip`
* A workstation PC configured with a TFTP server

**Procedure**

1. Clone the `automotive-image-builder` repository:

    ```console
    git clone https://gitlab.com/CentOS/automotive/src/automotive-image-builder.git
    ```

2. Build the R-Car S4 AutoSD image:

    ```console
    cd automotive-image-builder
    ./automotive-image-builder --container build --target rcar_s4 --mode package --export image <manifest> .
    ```

    Replace `<manifest>` with your custom OSBuild manifest file, in the `.mpp.yml` format. If you do not have your own manifest file, you can build
    an AutoSD image with the `automotive-image-builder/example.mpp.yml` manifest file.

3. Compress the image:

    ```console
    gzip cs9-rcar_s4-developer-regular.aarch64.img cs9-rcar_s4-developer-regular.aarch64.img.gz
    ```

4. Place this image into your TFTP server.

### Flashing the Image

To flash the AutoSD image onto your R-Car S4, you must copy the image from your TFTP server onto your board.

**Prerequisites**

* A workstation PC configured with a TFTP server that hosts your AutoSD image
* A network connection between the PC and the R-Car S4 board

**Procedure**

1. Download the image file to `0x480000000`:

    ```console
    tftp 0x480000000 cs9-rcar_s4-developer-regular.aarch64.img.gz
    ```

2. Copy the image to the MMC device `0`:

    ```console
    gzwrite mmc 0 0x480000000 ${filesize} 1000000 0
    ```

### Configuring U-Boot to Boot the Image

To boot the AutoSD image, you must configure U-Boot on the R-Car S4 to boot from the image.

**Prerequisites**

* The image is copied to MMC device `0`.

**Procedure**

1. Create an environment variable called `grub` that loads and starts EFI from partition 1 of the MMC device:

    ```console
    setenv grub 'fatload mmc 0:1 0x70000000 /EFI/BOOT/BOOTAA64.EFI && bootefi 0x70000000'
    setenv bootargs
    setenv bootcmd 'run grub'
    saveenv
    ```

    At this point, the board is configured to boot straight into AutoSD after reset.

2. To boot into AutoSD immediately, reset the board using the `reset` command.
3. To start the boot process manually, use the `run grub` command.

**Additional Resources**

* R-Car S4 Startup Guide, available from the
[R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway)
