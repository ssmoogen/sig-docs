# Automotive SIG Meeting: August 19, 2021

These are shared minutes for the inaugural meeting of the [CentOS Automotive SIG](https://sig.centos.org/automotive/).

Meeting link: was <https://meet.google.com/qqi-gzyc-wob>, WILL CHANGE for future meetings due to technical difficulties

Meeting time: August 19, 2021, 11am EDT (5pm CEST)

Updated note - the meeting was NOT recorded due to difficulties with the meeting software.

Please contact [jefro](mailto:jefro@jefro.net) with any questions or concerns. Slide content from the meeting is represented below.

## 1. Agenda

* Introduction to the Automotive SIG
* General goals for this SIG
* Specific goals through EOY 2021
* Decision-making process
* Establishment of community standards
* Nominations for chair and vice-chair
* Open discussion

## 2. Attendees

There were approximately 34 attendees from several organizations present, but a roll call was not taken.

## 3. Minutes

### 3.1. Introduction to the Automotive SIG

The purpose of the Automotive SIG is two-fold. First, it is meant to be a neutral public space for collaboration between third parties
interested in open development of software targeted at in-vehicle automotive use cases.
Second, it is meant to provide such projects with build and test infrastructure.

Slide content:

* Why an Automotive SIG in CentOS?
* Advancement of distro-based automotive Linux
* Focal point for working with other upstreams, hardware options, and related projects
* Public development for POCs, including the automotive CentOS variant
* Upstream resource & primary test bed for RHEL in an automotive context

### 3.2. General goals for this SIG

The general goal of the SIG is to provide an open-source home for distro-oriented automotive work (i.e. derived from binary artifacts,
rather than rebuilt from source), and to attract and encourage open development of automotive software between commercial and non-commercial partners.

To that end, this SIG will have three primary functions, as outlined in the slide.

Slide content:

* General Goals
  * Create open source software related to Linux in an automotive context
  * Incorporate and work directly with upstream projects related to automotive
  * Build and curate a [CentOS variant](https://www.centos.org/variants/) for Automotive based on [CentOS Stream](https://www.centos.org/centos-stream/),
    on a regular release schedule - manifest to be determined by SIG participants

In the meeting, we also talked frankly about Red Hat's motivation for creating this SIG, as it will be used as an upstream for RHEL
in an automotive context, preparatory to an eventual Red Hat automotive product.
Red Hat feels strongly that community development with the open source process is by far the best way to achieve success with this by working
collaboratively in this SIG and with other existing automotive Linux projects.
The intent is to augment existing efforts with a new design paradigm and to collaborate and share code whenever & wherever possible.

It was also discussed that this Automotive SIG can be a shining example of the way CentOS Stream works as a development process.

One question that came up was around privacy. CentOS is a public, fully transparent project, so any work that happens within the context of this SIG is
by definition public, and, according to the CentOS guidelines, must carry an open license approved by the CentOS board.
This applies to both code and documentation. If any work requires an NDA or other privacy protection, it must be done outside the scope of this SIG.

### 3.3. Specific goals through EOY 2021

The group talked through some practical goals to work toward over the next 4-5 months.

Slide content:

* Build a foundational community
  * Establish working relationships among all participants, and encourage others to join
  * Create robust community guidelines, including contribution guidelines and CoC
  * Vote administrative roles
  * Develop plans for the CentOS Automotive variant and build & test infrastructure
  * Presentation at Automotive Linux Summit (with stickers)

The group also wanted to discuss some concrete technical directions, particularly related to hardware support and encouraging silicon
providers to join and support this SIG (as well as AGL, Eclipse Automotive, and other open source automotive upstreams).
Many developers who are interested in automotive software do not have access to expensive development systems, so the discussion
focused on inexpensive hobbyist boards such as the NVIDIA Jetson and Raspberry Pi 4.

While hardware availability is a distinct but achievable challenge, the group also touched on access to safety manuals, which are a requirement preparatory
to functional safety certification. Both software products and hardware components will have their own manuals, but these have traditionally
been kept private to customers of those products due to concerns of liability as well as competition. As certification is actually based on a
certifier's rigor and adherence to the manual, a case can be made to create open safety manuals, though it is unknown whether hardware manufacturers
would be willing to do this.
Much of this discussion is also ongoing in the ELISA community.

### 3.4. Decision-making process

The group must decide first on a decision-making process.
Jefro's recommendation is to make it as simple, straightforward, and fair as possible, and for the group to revisit this
process in early 2022 when we have six months of collaboration behind us.

Some example questions to address: Do we want a heavy sense of governance, or do we all just want to pitch in where needed?
How will we decide which packages to incorporate, which hardware to support, and how to build and test these things?
The early participants are showing up with ideas, but we don’t want to leave anyone’s ideas out in the cold.

The short story is, we should decide on these issues together and not have a code of engagement handed down by any single organization.

### 3.5. Establishment of community standards

Community standards, and the communication of these standards, is the backbone of a healthy community.

Slide content:

* Communications
  * asynchronous: mailing lists
  * synchronous: IRC
  * periodic: meeting schedules & calendars
* Code of Conduct
  * abide by the CentOS Code of Conduct
  * discuss additional guidelines
* Administration and “leadership” roles

Several community communications channels have been established:

* [CentOS Automotive mailing list](https://lists.centos.org/hyperkitty/list/automotive-sig@lists.centos.org/)
* [CentOS Development mailing list](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/)
* `#centos-automotive` IRC channel on [Libera Chat](https://web.libera.chat/)
* monthly meetings on the third Thursday, 1500 UTC, or as decided by the group
* potential weekly "office hours" via IRC or on a separate call

The current meeting time is reasonable for Europe and the US, but not for Asia. As the group develops, it may be advantageous
to hold meetings at times more conducive to all times zones. To achieve this, we may move to alternating times and/or a divided
meeting that repeats on the same day, and to record meetings so people can catch up asynchronously.
We also discussed moving important topics to the mailing list so everyone has a chance to see them.

Rich let us know that the SIG is subject to the [CentOS Code of Conduct](https://www.centos.org/code-of-conduct/),
which outlines conduct expectations as well as an escalation path for anyone who feels uncomfortable. Jefro also has
had training in code of conduct resolution, so if anyone has any concerns or needs guidance on this subject,
please feel free to [send email to him personally](mailto:jefro@jefro.net) as a first step.
All communication on this subject will remain strictly confidential.

Rich also let us know that a chair position is required in order to have a point of contact between the SIG and the CentOS board.
The word "leadership" is in quotes to indicate that the chair position is administrative and not actually a set of power.
All decisions will be made by group consensus, in accordance with the CentOS SIG charter (and good community practice),
and no single organization will hold sway regardless of how many people show up.

Also related - the SIG must report status back to the CentOS board on a quarterly basis, which is one of the duties of the chair,
along with developing the agenda for this ongoing meeting and maintaining minutes.

## 3.6. Nominations for chair and vice-chair

Deferred for now. Jefro has agreed to act as the temporary chair until the group chooses someone for this rotating position.

## 3.7. Open discussion and Action Items

There were several discussion topics throughout the meeting as reported above.
These minutes are intended to be collaborative, so please feel free to add your thoughts.

Action items taken for the August meeting:

* fix the meeting invitation so everyone has the correct code (Jefro)
* continue to develop basic build/test infrastructure (Pierre-Yves)
* encourage all interested parties to join this community (everyone)
