# Community

This section contains all community related information, such as communication channels and meeting notes.

## Communication Channels

* [CentOS Automotive mailing list](https://lists.centos.org/hyperkitty/list/automotive-sig@lists.centos.org/)
* [CentOS Development mailing list](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/)
* [#centos-automotive-sig](https://app.element.io/#/room/#centos-automotive-sig:fedoraproject.org) on Matrix

## Meetings

The SIG supports a formal meeting monthly on the first Wednesday of the month, and an occasional informal "office hours" in the middle of the month.
Help is also available through the Matrix chat channel or mailing list.

This [Google calendar iCal link](https://calendar.google.com/calendar/ical/c_dacd5897e5065b4366d202df0a00d7b8b6b9c4e7adad24c0ff3cf25be7f99a06%40group.calendar.google.com/public/basic.ics)
contains the invitation to the meeting.
You can subscribe to it (recommended) or copy it into your own calendar.
For the latter, be aware that meeting changes are not automatically propagated to copies of the meeting.

If you would like to be added to the invite, please send email to [josiermi@redhat.com](mailto:josiermi@redhat.com).

CentOS Automotive SIG Google Meet joining info Video call link: [https://meet.google.com/hkx-btgd-dyc](https://meet.google.com/hkx-btgd-dyc)
Or dial: ‪(CA) +1 647-734-1769‬ PIN: ‪602 923 116‬#
More phone numbers: [https://tel.meet/hkx-btgd-dyc?pin=8112454766957](https://tel.meet/hkx-btgd-dyc?pin=8112454766957)
Or join via SIP: [8112454766957@gmeet.redhat.com](mailto:8112454766957@gmeet.redhat.com)

For convenience, there is a Google invitation for this meeting - contact [jefro@redhat.com](mailto:jefro@redhat.com) to be added to the calendar.
You may join anytime without being on the invitation.

### Past Meetings

<!-- TABLE LINKS START -->
[2023-08-02-rec1]: https://youtu.be/3Lvp59aCqT0
[2023-07-12-rec1]: https://youtu.be/3nb5tA_9uNw
[2023-07-12-doc1]: https://www.youtube.com/watch?v=kJsv07p_nm4
[2023-07-12-doc2]: https://www.youtube.com/watch?v=2VabVFmZ2cc
[2023-06-07-rec1]: https://youtu.be/lwbyy9huXJk
[2023-06-07-doc1]: https://iot.bzh/download/public/2023/IoT.bzh-Yocto-To-CentOS-Automotive_v1.0.pdf
[2023-05-03-rec1]: https://youtu.be/VXkSXZxFHaw
[2023-04-05-rec1]: https://youtu.be/PJ98HG40Rqc
[2023-03-08-rec1]: https://youtu.be/hB5Yd5M6tJg
[2023-02-01-rec1]: https://youtu.be/dtnxJe3TaZw
[2022-11-02-rec1]: https://youtu.be/sDjVAksfCZ4
[2022-10-05-rec1]: https://youtu.be/unxjseqmxjs
[2022-10-05-doc1]: https://gitlab.com/josiermi/centos-automotive-sig-slides/-/blob/main/CentOS_Automotive_SIG_-_October_5__2022.pptx
[2022-09-07-rec1]: https://youtu.be/VIAAK8GdLG0
[2022-08-03-rec1]: https://youtu.be/e92jCIx5GyQ
[2022-07-06-rec1]: https://youtu.be/bRDJn-RP_KU
[2022-06-01-rec1]: https://youtu.be/YUFSMLuLlm4
[2022-05-04-rec1]: https://youtu.be/owJAzKydveQ
[2022-04-08-rec1]: https://youtu.be/dGyzy0TNy-0
[2022-03-17-rec1]: https://youtu.be/uUcIeS0ibyo
[2022-03-02-rec1]: https://youtu.be/dqmNo212t48
[2022-02-02-rec1]: https://youtu.be/xC3eIRQM05s
[2021-12-15-rec1]: https://youtu.be/xqfFxj3bhiU
[2021-12-15-rec2]: https://youtu.be/DXiDD7otqI8
[2021-12-01-rec1]: https://www.youtube.com/watch?v=aOmxElin_18
[2021-12-01-rec2]: https://youtu.be/QBBgdFLlPrU
[2021-11-16-rec1]: https://youtu.be/Ew2xBrHM_3k
[2021-11-16-rec2]: https://youtu.be/PuhDB34px9o
[2021-10-21-rec1]: https://www.youtube.com/watch?v=L9xQBqWnSb4
[2021-10-21-doc1]: https://gitlab.com/josiermi/centos-automotive-sig-slides/-/blob/main/CentOS_Automotive_SIG_-_Oct_21__2021.pptx
[2021-10-01-rec1]: https://youtu.be/_9SngbXfjQY
[2021-09-16-rec1]: https://www.youtube.com/watch?v=b0pUDb_BWSQ
[2021-08-19-doc1]: innaugural_meeting_minutes.md
<!-- TABLE LINKS END -->

| Date               | Meeeting                 | Recording Link                     | Minutes/Slides Links                     |
| ------------------ | ------------------------ | ---------------------------------- | ---------------------------------------- |
| August 2, 2023     | monthly                  | [recording][2023-08-02-rec1]       |                                          |
| July 12, 2023      | monthly                  | [recording][2023-07-12-rec1]       | [AWS Demo][2023-07-12-doc1]              |
|                    |                          |                                    | [Toolbox demo][2023-07-12-doc2]          |
| June 7, 2023       | monthly                  | [recording][2023-06-07-rec1]       | [slides (iot.bzh)][2023-06-07-doc1]      |
| May 3, 2023        | monthly                  | [recording][2023-05-03-rec1]       |                                          |
| April 5, 2023      | monthly                  | [recording][2023-04-05-rec1]       |                                          |
| March 8, 2023      | monthly                  | [recording][2023-03-08-rec1]       |                                          |
| February 1, 2023   | monthly                  | [recording][2023-02-01-rec1]       |                                          |
| November 2, 2022   | monthly                  | [recording][2022-11-02-rec1]       |                                          |
| October 5, 2022    | monthly                  | [recording][2022-10-05-rec1]       | [slides][2022-10-05-doc1]                |
| September 7, 2022  | monthly                  | [recording][2022-09-07-rec1]       |                                          |
| August 3, 2022     | monthly                  | [recording][2022-08-03-rec1]       |                                          |
| July 6, 2022       | monthly                  | [recording][2022-07-06-rec1]       |                                          |
| June 1, 2022       | monthly                  | [recording][2022-06-01-rec1]       |                                          |
| May 4, 2022        | monthly                  | [recording][2022-05-04-rec1]       |                                          |
| April 8, 2022      | monthly                  | [recording][2022-04-08-rec1]       |                                          |
| March 17, 2022     | office hours             | [recording][2022-03-17-rec1]       |                                          |
| March 2, 2022      | monthly                  | [recording][2022-03-02-rec1]       |                                          |
| February 2, 2022   | monthly                  | [recording][2022-02-02-rec1]       |                                          |
| December 15, 2021  | monthly, US am           | [recording][2021-12-15-rec1]       |                                          |
|                    | monthly, US pm           | [recording][2021-12-15-rec2]       |                                          |
| December 1, 2021   | office hours, US am      | [recording][2021-12-01-rec1]       |                                          |
|                    | office hours, US pm      | [recording][2021-12-01-rec2]       |                                          |
| November 16, 2021  | monthly, US am           | [recording][2021-11-16-rec1]       |                                          |
|                    | monthly, US pm           | [recording][2021-11-16-rec2]       |                                          |
| October 21, 2021   | monthly                  | [recording][2021-10-21-rec1]       | [slides][2021-10-21-doc1]                |
| October 1, 2021    | office hours             | [recording][2021-10-01-rec1]       |                                          |
| September 16, 2021 | monthly                  | [recording][2021-09-16-rec1]       |                                          |
| August 19, 2021    | monthly                  | none                               | [minutes][2021-08-19-doc1]               |
